(*
 * Project: ideaPascal
 * User: USER
 * Date: 25/03/2017
 *)
program Sorting_buble_selection;

uses
    crt;

type
    larik = array[1..10] of byte;

    objek = object
        bykdata: byte;
        dataawal: larik;
        procedure input;
        procedure tukar(var a, b: byte);
        procedure bubble(Data: larik);
        procedure selection(Data: larik);
        procedure cetak(Data: larik);
    end;

procedure objek.input;
var
    I: byte;
begin
    repeat
        Write('Banyak data [max  10] ?');
        readln(bykdata);
        if (bykdata > 10) then
        begin
            writeln('Data yang dimasukkan kebanyakan ');
            readkey;
            writeln;
        end;
    until (bykdata <= 10);
    for i := 1 to bykdata do
    begin
        Write('Data ke - ', i, ' = ');
        readln(dataawal[i]);
    end;
end;

procedure objek.tukar(var a, b : byte);
var temp : byte;
begin
    temp := a; a:= b; b:=temp;
end;

procedure objek.bubble(Data: larik);
var
    i, j: byte;
begin
    clrscr;
    writeln('Buble sort');
    Write('Awal : ');
    cetak(Data);
    for j := 1 to bykdata - 1 do
    begin
        if (Data[j] > Data[j + 1]) then
            tukar(Data[j], Data[j + 1]);
        gotoxy(6, wherey);
    end;
    readkey;
    {end;     }

    Write('Akhir : ');
    cetak(Data);
    readkey;
end;

procedure objek.selection(Data: larik);
var
    i, j, lok: byte;
begin
    clrscr;
    writeln('Selection sort');
    Write('Awal ');
    cetak(Data);
    for i := 1 to bykdata do
    begin
        lok := i;
        for j := i + 1 to bykdata do
            if (Data[lok] > Data[j]) then
                lok := j;
        tukar(Data[i], Data[lok]);
        Write('I = ', i, ' lok= ', lok);
        cetak(Data);
    end;
end;

procedure objek.cetak(Data: larik);
var
    i: byte;
begin
    for i := 1 to bykdata do
    begin
        if(i=1) then
            gotoxy(i * 15, wherey)
        else
            gotoxy(i * 10, wherey);
        Write(Data[i]: 5);
    end;
    writeln;
end;

var
    sort: objek;
begin
    clrscr;
    sort.input;
    sort.bubble(sort.dataawal);
    sort.selection(sort.dataawal);
    readln;
end.
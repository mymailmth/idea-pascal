(*
 * Project: ideaPascal
 * User: USER
 * Date: 25/03/2017
 *)
program INSERTION_SWAP_SORT;
uses crt;
var
    i, j, k, temp, n : integer;
    a, b, c : array[1..100] of integer;

begin
    clrscr;
    textcolor(12);
    Write('Banyaknya Elemen Array : ');
    ReadLn(n);

    {Input Data}
    for i := 1 to n do
    begin
        Write('Elemen ke-', i , ' : ');
        ReadLn(A[i]);
    end;

    {Cetak Array Sebelum Pengurutan}
    WriteLn();
    textcolor(15);
    Write('Sebelum diurutkan : ');
    for i:= 1 to N do Write(A[i], ' ');

    WriteLn();textcolor(15); WriteLn();
    WriteLn('Proses Pengurutan Swap Insertion: ');
    for i:= 1 to n - 1 do begin
        j := i + 1;
        while (A[j] < A[j - 1]) and (j > 1) do
        begin
            temp := A[j - 1];
            A[j - 1] := A[j];
            A[J] := temp;
            J := j - 1;
        end;
        WriteLn();
        textcolor(i);

        Write('Hasil akhir langkah ke-', i, ' : ');
        {Cetak Array tiap langkah pengurutan : }
        for k := 1 to n do Write(A[K], ' ');
    end;

    {Cetak Array Setelah Pengurutan}
    WriteLn();
    WriteLn();
    textcolor(15);
    Write('Hasil Pengurutan Insertion : ');
    for i:= 1 to n do write(A[i], ' ');
    ReadLn();
end.
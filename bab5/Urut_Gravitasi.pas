(*
 * Project: ideaPascal
 * User: USER
 * Date: 18/03/2017
 *)
program Urut_Gravitasi;
uses crt;
var
    i, j, k, temp, n : integer;
    A, B, C :array[1..100] of Integer;

begin
    clrscr;
    textColor(12);
    Write('banyaknya Elemen Array : ');
    ReadLn(n);

    {input data}
    for i:= 1 to n do
    begin
        Write('Elemen ke-', i , ' : ');
        ReadLn(A[i]);
    end;

    {Cetak Array Sebelum Pengurutan}
    WriteLn;
    textcolor(15);
    Write('Sebelum diurutkan : ');
    for i:= 1 to n do
        Write(A[i] , ' ');
    WriteLn;
    textcolor(15);

    Writeln;
    WriteLn('Process pengurutan gravitasi : ');
    for i:= 1 to n - 1 do begin
        for j:= 1 to n - 1 do
        begin
            if a[j] > A[j + 1] then begin
                temp := A[j + 1];
                A[j + 1] := A[J];
                A[J] := temp;
            end;
        end;
        WriteLn;
        textcolor(i);
        {Cetak Array tiap langkah pengurutan:}
        Write('Hasil akhir langkah ke-', i, ' : ');
        for k:= 1 to n do
            Write(A[K], ' ');
    end;

    {Cetak Array Setelah Pengurutan}
    WriteLn;
    Writeln();
    textcolor(15);
    Write('Hasil pengurutan Gravitasi : ');
    for i:= 1 to n do
        Write(A[i], ' ');

    Writeln();
    WriteLn();
    textcolor(10);
    write('Sudah terurut dengan benar ');
    ReadLn();
end.
(*
 * Project: ideaPascal
 * User: USER
 * Date: 18/03/2017
 *)
program Urut_Seleksi;
uses crt;
var
    i, j, k , temp, imaks, n : Integer;
    A, B, C : array [1..100] of Integer;

begin
    clrscr;
    textcolor(10);
    Write('Banyaknya elemen array : ');
    ReadLn(n);

    {input Data }
    for i := 1 to n do begin
        Write('Elemen ke-', i, ': ');
        ReadLn(A[i]);
    end;

    {Cetak Array Sebelum Pengurutan }
    WriteLn()   ;
    textcolor(15);
    Write('Sebelum diurutkan : ');
    for i:= 1 to n do Write(A[i], ' ');
    WriteLn();
    WriteLn();
    textcolor(15);
    WriteLn('proses Pengurutan seleksi : ');
    for i:= 1 to n - 1 do
    begin
        iMaks := i;
        for j:= i + 1 to n  do
            if A[j] < A[imaks] then imaks := j;

        {Tukar A[j] dengan A[imaks]}
        temp := A[imaks];
        A[imaks] := A[i];
        A[i] := temp;
        WriteLn();
        textcolor(i);

        {Cetak Array tiap langkah pengurutan: }
        write('Hasil akhir langkah ke - ', i, ' : ');
        for k:= 1 to n do
            Write(A[k], ' ');
    end;


    {Cetak Array Setelah pengurutan: }
    textcolor(15); WriteLn(); Write('Hasil pengurutan Seleksi : ');
    for i := 1 to n do Write(A[i], ' ');
    ReadLn();
    {Hal 40}
end.
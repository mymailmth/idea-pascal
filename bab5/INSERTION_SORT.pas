(*
 * Project: ideaPascal
 * User: USER
 * Date: 18/03/2017
 *)
program INSERTION_SORT;
uses crt;

var
    i, j, k, temp, n : Integer;
    a, b, c : array[1..100] of integer;

begin
    clrscr;
    textcolor(12);
    Write('Banyaknya elemen array : ');
    ReadLn(n);

    {Input Data}
    for i:= 1 to n do begin
        Write('Elemen ke- ',i, ': ');
        ReadLn(A[i]);
    end;

    {Cetak Array Sebelum Pengurutan}


    {Hal 43}
    WriteLn();
    textcolor(15);
    Write('Sebelum diurutkan : ');
    for i:= 1 to n do
        Write(A[i], ' ');
    WriteLn();
    textcolor(15);
    WriteLn();
    WriteLn('Proses pengurutan insertion : ');
    for i:= 2 to n do begin
        temp := A[i];
        j := i - 1;
        while(temp < A[j]) and (j > 0) do
            j := j - 1;
        for k:= i downto j + 1 do
            A[k] := A[k - 1] ;
        A[j + 1] := temp;
        WriteLn();
        textcolor(i);
        Write('Hasil akhir langkah ke-', i - 1 , ' : ');

        {Cetak Array tiap langkah pengurutan :}
        for k:= 1 to n do
            write(A[k], ' ');

    end;

    {Cetak Array setelah pengurutan }
    WriteLn();
    WriteLn();
    textcolor(15);
    Write('Hasil Pengurutan Insertion ');
    for i:= 1 to n do
        Write(A[i], ' ');
    WriteLn();
    WriteLn();
    textcolor(10);
    Write('Sudah terurut dengan benar khan ? ');
    ReadLn();
end.
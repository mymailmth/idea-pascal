(*
 * Project: ideaPascal
 * User: USER
 * Date: 25/03/2017
 *)
program Sorting_swap_insertion;

uses
    crt;

type
    larik = array[1..10] of byte;

    Objek = object
        bykdata: byte;
        dataawal: larik;
        procedure input;
        procedure tukar(var a, b: byte);
        procedure swap_insertion(Data: larik);
        procedure cetak(Data: larik);
    end;

procedure objek.input;
var
    i: byte;
begin
    repeat
        Write('Banyak data [max 10] ? ');
        readln(bykdata);
        if (bykdata > 10) then
        begin
            writeln('Data yang dimasukkan kebanyakan ');
            readkey;
            writeln;
        end;
    until (bykdata <= 10);
    for i := 1 to bykdata do
    begin
        Write('Data ke-', i, '= ');
        readln(dataawal[i]);
    end;
end;

procedure objek.tukar(var a, b: byte);
var
    temp: byte;
begin
    temp := a;
    a := b;
    b := temp;
end;

procedure objek.swap_insertion(Data: larik);
var
    i, j, lok: byte;
begin
    clrscr;
    writeln(' Swap insertion sort ');
    Write('Awal ');
    cetak(Data);
    for i := 1 to bykdata - 1 do
    begin
        Write(' I  = ', i, ' ');
        j := i + 1;
    begin
        while (Data[j] < Data[j - 1]) and (j > 1) do
        begin
            tukar(Data[j], Data[j - 1]);
            j := j - 1;
            {gotoxy(6, wherey);}
            {write('J = ', J);}
            Cetak(data);
        end;
    end;
        writeln(' akhir = ');
        cetak(Data);
        readkey;
    end;
end;

procedure objek.cetak(Data: larik);
var
    i: byte;
begin
    for i := 1 to bykdata do
    begin
        gotoxy(i * 10, wherey);
        Write(Data[i]: 5);
    end;
    writeln;
end;

var
    sort: objek;

begin
    clrscr;
    sort.input;
    sort.swap_insertion(sort.dataawal);
end.
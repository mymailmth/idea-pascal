(*
 * Project: ideaPascal
 * User: USER
 * Date: 19/03/2017
 *)
Program  uts_2;

Uses crt;

Procedure Hitung(A, B, C: Integer);
begin
    C := A + B mod 2;
    writeln;
    writeln(' A=',A,' B=',B,' C=',C);
end;

var
    X, Y, Z : integer;
begin
    X := 4;
    Y := 8;
    Z := 0;
    hitung(X,Y,Z);
    writeln(' X=',X,' Y=',Y, ' Z=',Z);
end.

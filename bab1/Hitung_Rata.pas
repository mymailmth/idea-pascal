(*
 * Project: ideaPascal
 * User: USER
 * Date: 25/03/2017
 *)
program Hitung_Rata;
{Contoh Penggunaan Array dalam program}
var
    i, k, n : integer;
    Jumlah : LongInt;
    RATA : real;
    A : array [0..100] of Integer;
begin
    {Input Data}
    while A[n] <> -9 do
    begin
        n := n + 1;
        write('Masukkan bilangannya : ');
        ReadLn(A[n]);
    end;
    n:= n-1;
    WriteLn('Banyaknya bilangan : ', n);
    {Mencetak Elemen Array}
    Write('Elemen array-nya adalah : ');
    for i:= 1 to n do
    Write(A[i], ' , '); WriteLn();

    {Menghitung Jumlah Elemen array}
    for i:= 1 to n do Jumlah := Jumlah+A[i];
    Write('Jumlah akhir = ', Jumlah);

    {Menghitung rata-rata}
    RATA := Jumlah/n;
    WriteLn(' dan rata rata = ', RATA);
    WriteLn('--o SELESAI o--');
    ReadLn();
end.
(*
 * Project: ideaPascal
 * User: mtoha 201643570041
 * Date: 16/04/2017
 *)
program RecordSort;
Type
    DataMhs = Record
        NPM : integer;
        Nama : String[20];
        IP : real;
    end;
var
    JumlahMhs, I, J : byte;
    Mahasiswa : array[1..50] of DataMhs;
    Temp: DataMhs;
begin
    {Masukkan data  Mahasiswa}
    Write('Jumlah Mahasiswa ? ');
    ReadLn(JumlahMhs);
    for I := 1 to JumlahMhs do begin
        WriteLn();
        with Mahasiswa[i] do begin
            WriteLn();
            Write('NPM ke ', i:2, '?');
            ReadLn(NPM);
            Write('Nama Mahasiswa Ke ', i:2,'?' ); ReadLn(Nama);
            Write('IP Mahasiswa ke ', i:2, '?');
            ReadLn(IP);
        end;
    end;
    {Mengurutkan data berdasarkan NPM Mahasiswa dengan Bubble Sort}
    for I :=  1 to JumlahMhs - 1 do
        for J:= 1 to JumlahMhs - i do
            if Mahasiswa[J].NPM > Mahasiswa[J + 1 ].NPM then
            begin
                Temp := Mahasiswa[j];
                Mahasiswa[j] := Mahasiswa[J + 1];
                Mahasiswa[J + 1] := Temp;
            end;

    {Menampilkan Hasil}
    WriteLn();
    WriteLn('--------------------------------------------------------------------');
    WriteLn('NPM            Nama Mahasiswa                                      IP');
    WriteLn('--------------------------------------------------------------------');
    for i:= 1 to JumlahMhs do begin
        with  Mahasiswa[i] do
        WriteLn(NPM:5, Nama: 20, IP : 11:2);
    end;
    WriteLn('--------------------------------------------------------------------');
    ReadLn();
end.

(*
 * Project: ideaPascal
 * User: mtoha 201643570041
 * Date: 16/04/2017
 *)
program RecordSample;
type
    tglLhr= record
        hari: string[31];
        bulan: string[12];
        tahun: string[12];
    end;
    almt= record
        jalan : string[35];
        kota : string[25];
    end;
    mhs= record
        nama: string[25];
        alamat: almt;
        tglLahir: tglLhr;
    end;
var
    dataMhs: Mhs;ss
begin
    dataMhs.nama := 'intan';
    dataMhs.alamat.jalan := 'muara 2';
    dataMhs.alamat.kota := 'jakarta';
    dataMhs.tglLahir.hari := '19';
    dataMhs.tglLahir.bulan := '11';
    dataMhs.tglLahir.tahun := '1992';
    with dataMhs do
    begin
        writeln('nama mahasiswa =',nama);
        with alamat do
        begin
            writeln('alamat  =',jalan);
            writeln('     ',kota);
        end;
        with tglLahir do
        begin
            writeln('tanggal lahir =',hari:2,'-',bulan:2,'-',tahun:4);
        end;
    end;
end.
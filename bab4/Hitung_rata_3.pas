(*
 * Project: ideaPascal
 * User: USER
 * Date: 18/03/2017
 *)
program Hitung_rata_3;
var
    k, n, maksimum, minimum : integer;
    jumlah : LongInt;
    rata : Real;
    A : array[0..100] of Integer;

procedure InputData;
begin
    while A[n] <> -9 do begin
        n := n + 1;
        write('Masukkan bilangannya : ');
        readln(A[n]);
    end; {End of While}
    n := n - 1;
end; {End of procedure}
procedure CetakArray;
var
    i : integer;
begin
    for i := 1 to n do write(A[i], ', ');
end;

function Total(B : array of integer;
        X: integer): longint ;
var
    i : integer;
    temp : longint;
begin
    temp := 0;
    for i := 1 to X do temp := temp + b[i];
    Total := temp;
end;{end of Function}

function MIN(B: Array of integer ; X: integer): integer;
var
    i : Integer;
    temp : integer;
begin
    temp := b[1];
    for i:= 2 to X do if temp > b[i] then temp := B[i];
    min := temp;
end;

function Max(D: array of integer; N : integer): integer;
var
    i : integer;
    temp : integer;
begin
    temp := d[1];
    for i:= 2 to N  do if temp < d[i] then temp := D[i];
    max := temp;
end;


begin
    InputData;
    WriteLn('Banyaknya bilangan : ', n);
    Write('Elemen array-nya adalah : ');
    CetakArray;
    writeln;
    jumlah := Total(A,n);
    write('Jumlah akhir = ', jumlah);
    rata := jumlah / n;
    WriteLn(' dan rata-rata = ', rata);
    minimum := MIN(A,n);
    WriteLn('Elemen Terkecil = ', minimum);
    maksimum := Max(A, n);
    WriteLn('Elemen Terbesar = ', maksimum);

    WriteLn('--o Selesai o--');
    ReadLn;
end.
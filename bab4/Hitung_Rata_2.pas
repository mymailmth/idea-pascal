(*
 * Project: ideaPascal
 * User: USER
 * Date: 01/04/2017
 *)
program Hitung_Rata_2;
{Contoh Penggunaan Procedure dan Array}
var
    k, n : Integer;
    Jumlah : LongInt;
    Rata : Real;
    A : array[0..100] of Integer;

procedure  InputData;
begin
    while A[n] <> -9 do begin
        n := n + 1;
        write('Masukkan bilangannya : ');
        readln(A[n]);
    end; {end of while}
n := n - 1;
end;

procedure  CetakArray;
var i : integer;
begin
    for i:= 1 to n do Write(A[i] ,', ' );
end;

procedure HitungJumlah(var Total:longint);
var i:integer;
begin
    for i:= 1 to n do
        Total := Total + A[i];
end; {End of procedure}

begin {Program utama}
    InputData(); {Panggil prosedur InputData}
    WriteLn('Banyaknya bilangan : ', n);
    Write('Elemen array-nya adalah : ');
    CetakArray(); {Panggil prosedur CetakArray}
    WriteLn();
    HitungJumlah(Jumlah); {Panggil HitungJumlah}
    Write('Jumlah akhir = ', Jumlah);
    Rata := Jumlah/n;
    WriteLn(' dan rata-rata = ', Rata);
    WriteLn('--o SELESAI o--');
end.
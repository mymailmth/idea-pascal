(*
 * Project: ideaPascal
 * User: USER
 * Date: 13/04/2017
 *)
program MERGING_ARRAY;
uses crt;
var
    i, j, k, m, n, y, Temp : Integer;
    A, B, C : array[0..100] of Integer;

procedure Cetak_array(A: array of integer; x: integer);
var
    i: integer;
begin
    Write('Hasilnya : ');
    for i:= 0 to x - 1 do Write(A[i], ' ');
end;

procedure sisip(var A : array of integer;
        x: integer);
var
    i, j , temp : integer;
begin
    for i:= 1 to x - 1 do begin
        temp := A[i];
        j := i;
        while (temp < A[j - 1]) and (j > 0 ) do j := j - 1;

        for k:=i downto j do
            A[k] := a[k - 1];
        A[J] := temp;
    end;
end;
{Hal  55}
procedure  Gabung(A, B array of integer);
var
    c: array of integer;
var
    x: integer;
var
    i, j, k, y : integer;
begin
    i := 0;
    j := 0;
    k := 0;
    while (i <= m ) and (j <= n) do begin
        if a[i] < B[j] then begin
            C[k] := A[i];
            i := i + 1;
        end
        else begin
            C[k] := B[j];
            j := j + 1;
        end;
        k := k + 1;
        write ('i = ', i, ' j=', j, ' k=', k, ' ');
        cetak_array(C, k - 1);
        writeln;
    end;
    if i > m then begin
        for y:= k - 1 to m + n do begin
            C[y] := B[j];
            j := j + 1;
        end;
    end
    else if j > n then begin
        for y:= k - 1 to m + n do begin
            C[y] := A[i];
            i := i + 1;
        end;
    end;
    write('Setelah penggabungan : ');
    cetak_array(C, y);
    x := y;
end;

begin
    clrscr;
    randomize;
    WriteLn(' MERGING');
    Write('Banyaknya elemen array pertama : ');
    ReadLn(m);
    Write('Banyaknya elemen array kedua : ');
    ReadLn(n);
    for i:= 1 to m do
        A[i] := random(100);
    for i := 1 to n do
        B[i] := random(100);
    WriteLn('Sebelum penggabungan : ');
    sisip(A,m);
    Write(' Array pertama : ');
    Cetak_array(A, m);
    WriteLn();
    sisip(B, n );
    Write('Array kedua : ');
    Cetak_array(B,n);
    WriteLn();
    WriteLn('Proses Penggabungan : ');
    Gabung(A,B, m, n, C, y);
    WriteLn();
    Write('Setelah penggabungan : ');
    Cetak_array(C,y);
    ReadLn();
end.

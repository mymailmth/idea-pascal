program binary_search;

uses crt;
TYPE
    TIPE_larik = word;
    larik = array[1..8] of tipe_larik;

procedure cari_biner(x: larik; cari: tipe_larik; bawah, atas: word; var urutKetemu: word);
var
    tengah: word;
begin
    if bawah > atas then urutketemu := 0
    else begin
        tengah := (bawah + atas) div 2;
        if cari = X[tengah] then
            urutketemu := tengah
        else
            if cari < x[tengah] then
                cari_biner(x,cari, bawah, tengah - 1, urutketemu)
            else
                cari_biner(x,cari, tengah + 1, atas, urutketemu);
    end;
end;
type
    nama= string[20];
const
    npm: larik = (1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241);
    nama_mhs: array[1..8] of nama = ('arief', 'ani', 'budi', 'citra', 'dewi', 'erni', 'fanny' , 'ghana');

var
    cari: word;
    ketemu: word;
    lagi: char;

begin
    lagi := 'Y';
    while upcase(lagi) = 'Y' do
    begin
        clrscr;
        write('npm mahasiswa yg dicari: ');
        readln(cari);
        writeln;
        cari_biner(npm, cari,1,8 ,ketemu);
        if ketemu = 0 then begin
            writeln('npm mahasiswa ini tidak ada');
        end
        else begin
            writeln('npm mahasiswa : ' , npm[ketemu]);
            writeln(' nama mhs : ', nama_mhs[ketemu]);
        end;
        writeln;
        write('cari yg lain? Y/T ?');
        readln(lagi);
    end ;
end.